import csv
import pyalex
from pyalex import Works, Authors, Sources, Institutions, Concepts, Publishers
import os
import json

import config

if not config or not config.email:
    print(
        "You should add a config file copied from config.py.sample and add your email to activate polite pool."
    )
else:
    print("Polite pool activated")
    # configure polite pool
    pyalex.config.email = config.email

os.makedirs("./data/exploration", exist_ok=True)

# resume
done_queries = set()
if os.path.exists("./data/exploration/queries_overview.csv"):
    with open("./data/exploration/queries_overview.csv", "r") as f:
        for q in csv.DictReader(f):
            done_queries.add(q["query"])
print(f"resuming {len(done_queries)} queries")
with open("./data/search_queries.csv", "r") as input_queries_f, open(
    "./data/exploration/queries_overview.csv", "a"
) as output_queries_f:
    results = csv.DictWriter(
        output_queries_f,
        [
            "query",
            "works_count",
            "works_first_score",
            "works_last_score",
            "concepts_count",
            "concepts_first_score",
            "concepts_first_id",
            "works_with_concept_count",
        ],
    )
    if not os.path.exists("./data/exploration/queries_overview.csv"):
        results.writeheader()

    for query in csv.DictReader(input_queries_f):
        if query["query"] not in done_queries:
            print(query["query"])
            meta_works_with_concepts = None

            # search works
            first_page_works, meta_works = (
                Works().search(query["query"]).get(per_page=200, return_meta=True)
            )
            if meta_works["count"] > 0:
                with open(f'./data/exploration/{query["query"]}.json', "w") as f:
                    json.dump(first_page_works, f, indent=2)
            # search concepts
            first_page_concepts, meta_concepts = (
                Concepts().search(query["query"]).get(per_page=200, return_meta=True)
            )
            if (
                meta_concepts["count"] > 0
                and first_page_concepts[0]["relevance_score"] > 10
            ):
                _, meta_works_with_concepts = (
                    Works()
                    .filter(concepts={"id": first_page_concepts[0]["id"]})
                    .get(per_page=1, return_meta=True)
                )

            result = {
                "query": query["query"],
                "works_count": meta_works["count"],
                "works_first_score": first_page_works[0]["relevance_score"]
                if len(first_page_works) > 0
                else "",
                "works_last_score": first_page_works[-1]["relevance_score"]
                if len(first_page_works) > 0
                and "relevance_score" in first_page_works[-1]
                else "",
                "concepts_count": meta_concepts["count"],
                "concepts_first_score": first_page_concepts[0]["relevance_score"]
                if len(first_page_concepts) > 0
                else "",
                "concepts_first_id": first_page_concepts[0]["id"]
                if len(first_page_concepts) > 0
                else "",
                "works_with_concept_count": meta_works_with_concepts["count"]
                if meta_works_with_concepts
                else 0,
            }
            results.writerow(result)
