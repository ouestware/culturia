const { parse } = require("csv-parse");
const gexf = require("graphology-gexf");
const { DirectedGraph } = require("graphology");
const { writeFileSync, createReadStream } = require("fs");
const { forEachConnectedComponent } = require("graphology-components");
const { subgraph } = require("graphology-operators");

const graph = new DirectedGraph();

const parser = parse({ delimiter: ",", columns: true });
let nb = 0;

parser.on("readable", function () {
  let row;
  while ((row = parser.read()) !== null) {
    nb += 1;

    const r = graph.mergeNode(row.openalex_id, row);
    if (row.parent_ids != "")
      row.parent_ids.split(", ").forEach((parent) => {
        if (parent != "")
          graph.mergeEdge(row.openalex_id, parent.toLowerCase());
      });
  }
});

parser.on("end", function () {
  console.log(nb);
  forEachConnectedComponent(graph, (component) => {
    if (component.includes("https://openalex.org/c154945302")) {
      console.log("exporting graph", component.length);
      writeFileSync(
        "../data/concepts_tree.gexf",
        gexf.write(subgraph(graph, component))
      );
    }
  });
});

createReadStream("../data/openalex_concepts.csv").pipe(parser);
