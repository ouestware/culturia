export interface OpenAlexConcept {
  id: string;
  display_name: string;
  works_count: number;
  hint?: string;
}

export interface OpenAlexConceptInWork {
  id: string;
  wikidata: string;
  display_name: string;
  level: number;
  score: number;
}

export interface OpenAlexWork {
  id: string;
  title: string;
  relevance_score: number;
  publication_year: number;
  type: string; //actually it's an enum
  cited_by_count: number;
  concepts: OpenAlexConceptInWork[];
  abstract_inverted_index: Record<string, number[]>;
}

export interface OpenAlexApiResponse<OpenAlexResource> {
  group_by?: any[];
  meta: {
    count: number;
    db_response_time_ms: number;
    page: number;
    per_page: number;
  };
  results: OpenAlexResource[];
}
