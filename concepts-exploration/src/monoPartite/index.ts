import Graph, { UndirectedGraph } from "graphology";
import { isBipartiteBy } from "graphology-bipartite";
import { intersection, keys, max, min, omit, sum, values } from "lodash";

/**
 * jaccard
 * Ruzicka: weighted jaccard
 */

type Vector = Record<string, number>;

const METRICS = {
  naive: (v1: Vector, v2: Vector): number => {
    const intersectionLength = intersection(keys(v1), keys(v2)).length;
    return intersectionLength;
  },
  jaccard: (v1: Vector, v2: Vector): number => {
    const intersectionLength = intersection(keys(v1), keys(v2)).length;
    return (
      intersectionLength /
      (keys(v1).length + keys(v2).length - intersectionLength)
    );
  },
  ruzicka: (v1: Vector, v2: Vector): number => {
    const intersectionV1V2 = intersection(keys(v1), keys(v2));
    return (
      sum(intersectionV1V2.map((k) => min([v1[k], v2[k]]))) /
      sum(intersectionV1V2.map((k) => max([v1[k], v2[k]])))
    );
  },
  pmi: (v1: Vector, v2: Vector): number => {
    const intersectionV1V2 = intersection(keys(v1), keys(v2));
    return Math.log(
      intersectionV1V2.length / (keys(v1).length * keys(v2).length)
    );
  },
  cosine: (v1: Vector, v2: Vector): number => {
    const intersectionV1V2 = intersection(keys(v1), keys(v2));
    return (
      // dot product
      sum(intersectionV1V2.map((k) => v1[k] * v2[k])) /
      // magnitude
      Math.sqrt(
        sum(values(v1).map((val) => Math.pow(val, 2))) *
          sum(values(v1).map((val) => Math.pow(val, 2)))
      )
    );
  },
} as const;
type SimilarityMetricName = keyof typeof METRICS;

export interface MonoPartiteSettings {
  partiteAttribute: string;
  partiteToKeep: string;
  graph: Graph;
  similarityMetric?: SimilarityMetricName;
  weightPartiteAttribute?: string;
  similarityAttribute?: string;
  similarityThreshold?: number;
  keepPartiteAttribute?: boolean;
}

const monoPartite = ({
  partiteAttribute,
  partiteToKeep,
  graph,
  weightPartiteAttribute,
  similarityMetric,
  similarityAttribute,
  similarityThreshold,
  keepPartiteAttribute,
}: MonoPartiteSettings): Graph | null => {
  if (isBipartiteBy(graph, partiteAttribute)) {
    const vectors: Record<string, Record<string, number>> = {};
    const monoPartiteGraphe = new UndirectedGraph();

    /** getNodeVector
     * Build the vector used to comput similarity
     * This vector is composed of neighbors, weighted if weightPartiteAttribute is set
     * @param node
     * @returns Record<string, number>
     */
    const getNodeVector = (node: string) => {
      if (!vectors[node]) {
        vectors[node] = graph.reduceEdges(
          node,
          (acc, _, edgeAttrs, source, target) => {
            const otherPart = node === source ? target : source;
            return {
              ...acc,
              [otherPart]: weightPartiteAttribute
                ? edgeAttrs[weightPartiteAttribute]
                : 1,
            };
          },
          {}
        );
      }
      return vectors[node];
    };

    // build monoPartite by iterate on partToKeep nodes
    graph.forEachNode((node, attrs) => {
      if (attrs[partiteAttribute] === partiteToKeep) {
        const nodeVector = getNodeVector(node);
        monoPartiteGraphe.mergeNode(
          node,
          keepPartiteAttribute ? attrs : omit(attrs, partiteAttribute)
        );

        keys(nodeVector).forEach((otherPart) => {
          graph.forEachEdge(
            otherPart,
            (edge, edgeAttrs, source, target, sourceAttrs, targetAttrs) => {
              const [node2, node2Attrs] =
                otherPart === source
                  ? [target, targetAttrs]
                  : [source, sourceAttrs];
              // < to compute only the first half matrice != to avoid auto-link
              if (!monoPartiteGraphe.hasNode(node2) && node2 < node) {
                const node2Vector = getNodeVector(node2);
                monoPartiteGraphe.mergeNode(
                  node2,
                  keepPartiteAttribute
                    ? node2Attrs
                    : omit(node2Attrs, partiteAttribute)
                );

                const similarity = METRICS[similarityMetric || "naive"](
                  nodeVector,
                  node2Vector
                );
                if (!similarityThreshold || similarity >= similarityThreshold)
                  monoPartiteGraphe.addEdge(node, node2, {
                    [similarityAttribute || "similarity"]: similarity,
                  });
              }
            }
          );
        });
      }
    });

    return monoPartiteGraphe;
  }
  throw new Error(`Graph is not bipartite on ${partiteAttribute}`);
};

export default monoPartite;
