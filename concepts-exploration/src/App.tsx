import axios from "axios";
import { scaleLinear, scalePow } from "d3-scale";
import { UndirectedGraph } from "graphology";
import { circular } from "graphology-layout";
import { pick, sortBy, toPairs } from "lodash";
import { useEffect, useState } from "react";
import { BiSearchAlt } from "react-icons/bi";
import { useSearchParams } from "react-router-dom";

import ConceptsSelect from "./components/ConceptsSelect";
import { DisplayGraph } from "./components/Sigma";
import monoPartite from "./monoPartite";
import { OpenAlexApiResponse, OpenAlexConcept, OpenAlexWork } from "./types";

function App() {
  let [searchParams, setSearchParams] = useSearchParams();
  const [newUrlSearchParams, setNewUrlSearchParams] =
    useState<URLSearchParams>(searchParams);

  const [query, setQuery] = useState<string>("");
  const [conceptsFilter, setConceptsFilter] = useState<OpenAlexConcept[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [data, setData] = useState<OpenAlexApiResponse<OpenAlexWork> | null>(
    null
  );
  const [workConceptGraph, setWorkConceptGraph] =
    useState<UndirectedGraph | null>(null);
  const [conceptsGraph, setConceptsGraph] = useState<UndirectedGraph | null>(
    null
  );

  // LOAD STATE FROM URL AND LOAD DATA
  useEffect(() => {
    const trackInMatomo = (
      _query: string,
      _conceptsFilter: OpenAlexConcept[],
      _nb: number
    ) => {
      const matomoTracker = (
        window as unknown as {
          _paq: {
            push: (p: (string | boolean | number)[]) => void;
          };
        }
      )._paq;
      if (matomoTracker) {
        const fakeKeywords =
          _query !== "" || _conceptsFilter.length > 0
            ? `${_query} concepts:${sortBy(
                _conceptsFilter.map((c) => c.display_name)
              ).join(", ")}`
            : "";
        matomoTracker.push([
          "trackSiteSearch",
          // Search keyword searched for
          fakeKeywords,
          // Search category selected in your search engine. If you do not need this, set to false
          false,
          // Number of results on the Search results page. Zero indicates a 'No Result Search Keyword'. Set to false if you don't know
          _nb,
        ]);
      }
    };

    const getWorks = async (query: string, concepts: OpenAlexConcept[]) => {
      if (query !== "" || concepts.length > 0) {
        setData(null);
        setIsLoading(true);

        const filters = [];
        if (query !== "") filters.push(`default.search:${query}`);
        if (concepts.length > 0)
          filters.push(`concepts.id:${concepts.map((c) => c.id).join("|")}`);
        try {
          const response = await axios.get<OpenAlexApiResponse<OpenAlexWork>>(
            "https://api.openalex.org/works",
            {
              method: "GET",
              params: {
                filter: filters.join(","),
                mailto: `****@****.com`,
                "per-page": 200,
              },
            }
          );
          if (response.data) {
            console.log(
              `got ${response.data.results.length} on ${response.data.meta.count}`
            );
            trackInMatomo(query, concepts, response.data.meta.count);
            setData(response.data);
          }
        } finally {
          setIsLoading(false);
        }
      }
    };
    console.log("new Search Params", searchParams.toString());
    const query = searchParams.get("q") || "";
    setQuery(query);
    const conceptIdsAsString = searchParams.get("c");
    const conceptIds = conceptIdsAsString
      ? conceptIdsAsString.split("|")
      : null;
    if (conceptIds) {
      Promise.all(
        conceptIds.map((id) =>
          axios
            .get<OpenAlexConcept>(`https://api.openalex.org/concepts/${id}`)
            .then((response) => response.data)
        )
      ).then((concepts) => {
        setConceptsFilter(concepts);
        getWorks(query, concepts);
      });
    } else {
      getWorks(query, []);
    }
  }, [searchParams, setConceptsFilter]);

  // PROCESS DATA FROM OPEN ALEX
  useEffect(() => {
    if (data && data.results) {
      // compute graph
      const graph = new UndirectedGraph();
      const results =
        query !== ""
          ? data.results.filter((r) => r.relevance_score !== undefined)
          : data.results;
      const relevanceDomaine =
        query !== "" && results.length > 0
          ? [results[0].relevance_score, results.slice(-1)[0].relevance_score]
          : [1, 1];
      const scaleWorkSize = scaleLinear()
        .domain(relevanceDomaine)
        .range([2, 10]);
      const scaleEdge = scalePow().domain([0, 1]).range([0.1, 2]);
      results.forEach((w) => {
        graph.addNode(w.id, {
          label: w.title,
          color: "#a4bc71",
          size: scaleWorkSize(w.relevance_score || 0.5),
          nodeType: "work",
          workType: w.type,
          ...pick(w, ["publication_year", "title", "relevance_score"]),
        });
        w.concepts.forEach((c) => {
          if (c.score > 0.3) {
            graph.updateNode(c.id, (attr) => ({
              label: c.display_name,
              color: "#c37fab",
              nodeType: "concept",
              level: c.level,
              wikidata: c.wikidata,
              relevance: c.score + (attr.score || 0),
              degree: (attr.degree || 0) + 1,
            }));
            graph.addEdge(w.id, c.id, {
              score: c.score,
              size: scaleEdge(c.score),
            });
          }
        });
      });
      const conceptSizeDomain = graph.reduceNodes(
        (acc: [number, number], _, attrs): [number, number] => {
          return attrs.nodeType === "concept"
            ? [
                acc[0] > attrs.relevance ? (attrs.relevance as number) : acc[0],
                acc[1] < attrs.relevance ? (attrs.relevance as number) : acc[1],
              ]
            : acc;
        },
        [Infinity, -Infinity]
      );
      console.log(conceptSizeDomain);
      const scaleConceptSize = scaleLinear()
        .domain(conceptSizeDomain)
        .range([1, 10]);
      graph
        .filterNodes((n, { nodeType }) => nodeType === "concept")
        .forEach((c) => {
          const { relevance } = graph.getNodeAttributes(c);

          graph.setNodeAttribute(c, "size", scaleConceptSize(relevance));
        });
      circular.assign(graph);
      setWorkConceptGraph(graph);
      // monopartite on concepts
      const _conceptsGraph = monoPartite({
        graph,
        partiteAttribute: "nodeType",
        weightPartiteAttribute: "score",
        partiteToKeep: "concept",
        similarityMetric: "ruzicka",
        //similarityThreshold: 0.3,
        keepPartiteAttribute: true,
      });
      if (_conceptsGraph) {
        _conceptsGraph?.forEachEdge((e, attrs) => {
          _conceptsGraph.setEdgeAttribute(e, "size", attrs.similarity * 10);
        });
        circular.assign(_conceptsGraph);
      }

      setConceptsGraph(_conceptsGraph);
    } else {
      setWorkConceptGraph(null);
      setConceptsGraph(null);
    }
    // eslint-disable-next-line
  }, [data]);

  useEffect(() => {
    const newUrlSearchParams = new URLSearchParams(searchParams);
    if (query !== "") newUrlSearchParams.set("q", query);
    else newUrlSearchParams.delete("q");
    if (conceptsFilter.length > 0)
      newUrlSearchParams.set(
        "c",
        sortBy(
          conceptsFilter.map((c) => c.id.replace("https://openalex.org/", ""))
        ).join("|")
      );
    else newUrlSearchParams.delete("c");
    setNewUrlSearchParams(newUrlSearchParams);
  }, [searchParams, query, conceptsFilter]);

  return (
    <div className="container bg-light p-5">
      <div className="row col-12">
        <h1>Explore OpenAlex concepts</h1>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            setData(null);
            setSearchParams(newUrlSearchParams.toString());
          }}
        >
          <div className="row mb-4">
            <div>
              <label htmlFor="query">
                Search for words in works' title, abstract and full-text
              </label>
              <input
                id="query"
                placeholder="Type words to be searched in works"
                type="search"
                className="form-control"
                value={query}
                onChange={(e) => {
                  setQuery(e.target.value);
                }}
              />
            </div>
            <div>
              <label htmlFor="conceptsFilter">
                Restrict search to works tagged with one those concepts
              </label>
              <ConceptsSelect
                id="conceptsFilter"
                concepts={conceptsFilter}
                onChange={(concepts) => {
                  setConceptsFilter(concepts);
                }}
              />
            </div>

            <div className="mt-2">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={
                  newUrlSearchParams?.toString() === searchParams.toString()
                }
              >
                <BiSearchAlt width="1rem" /> Search
              </button>
            </div>
          </div>
        </form>
      </div>
      {isLoading && (
        <div className="row d-flex m-3 justify-content-center">
          <div className="spinner-border" role="status">
            <span className="invisible sr-only">Loading...</span>
          </div>
        </div>
      )}
      {conceptsGraph && (
        <div className="row">
          {" "}
          <h2>Concepts cooccurrences in works</h2>
          <DisplayGraph graph={conceptsGraph} />
        </div>
      )}
      {workConceptGraph && (
        <div className="row">
          {" "}
          <h2>Work - Concepts</h2>
          <DisplayGraph graph={workConceptGraph} />
        </div>
      )}
      {data && (
        <div>
          <h2>Works</h2>
          {data.results.map((w) => {
            const title = (
              <>
                {w.title} ({w.publication_year}) -{" "}
                <i className="text-muted">score: {w.relevance_score}</i>
              </>
            );
            return w.abstract_inverted_index ? (
              <details key={w.id} className="mb-2">
                <summary className="fs-4">
                  {w.title} ({w.publication_year}) -{" "}
                  <i className="text-muted">score: {w.relevance_score}</i>
                </summary>
                {sortBy(
                  toPairs(w.abstract_inverted_index).reduce((list, p) => {
                    const [term, indices] = p;
                    const flat = indices.map((i) => {
                      return [term, i] as [string, number];
                    });
                    return [...list, ...flat];
                  }, [] as [string, number][]),
                  ([_, i]) => i
                )
                  .map(([term]) => term)
                  .join(" ")}
              </details>
            ) : (
              <div className="fs-4 mb-2" key={w.id}>
                {title}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}

export default App;
