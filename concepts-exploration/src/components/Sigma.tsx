import {
  ControlsContainer,
  FullScreenControl,
  SearchControl,
  SigmaContainer,
  ZoomControl,
} from "@react-sigma/core";
import "@react-sigma/core/lib/react-sigma.min.css";
import { LayoutForceAtlas2Control } from "@react-sigma/layout-forceatlas2";
import Graph from "graphology";
import forceAtlas2 from "graphology-layout-forceatlas2";
import { FC } from "react";
import { SigmaSettings } from "./SigmaSettings";

export const DisplayGraph: FC<{ graph: Graph }> = ({ graph }) => {
  return (
    <SigmaContainer style={{ height: "60vh", width: "100%" }} graph={graph}>
      <SigmaSettings />
      <ControlsContainer position={"bottom-right"}>
        <ZoomControl />
        <FullScreenControl />
        <LayoutForceAtlas2Control
          settings={{
            settings: {
              ...forceAtlas2.inferSettings(graph),
            },
          }}
        />
      </ControlsContainer>
      <ControlsContainer position={"top-right"}>
        <SearchControl style={{ width: "200px" }} />
      </ControlsContainer>
    </SigmaContainer>
  );
};
