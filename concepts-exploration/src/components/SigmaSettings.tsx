import { useRegisterEvents, useSetSettings, useSigma } from "@react-sigma/core";
import { FC, useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { NodeDisplayData } from "sigma/types";

export const SigmaSettings: FC = () => {
  const sigma = useSigma();
  const registerEvents = useRegisterEvents();
  const setSettings = useSetSettings();
  const [, setSearchParams] = useSearchParams();

  const [hoveredNode, setHoveredNode] = useState<string | null>(null);

  useEffect(() => {
    // register events
    registerEvents({
      enterNode: (event) => {
        setHoveredNode(event.node);
        sigma.getContainer().classList.add("pointer");
      },
      leaveNode: () => {
        setHoveredNode(null);
        sigma.getContainer().classList.remove("pointer");
      },
      clickNode: (event) => {
        const nodeType = sigma
          .getGraph()
          .getNodeAttribute(event.node, "nodeType");

        if (nodeType === "concept") {
          setSearchParams(
            `c=${event.node.replace("https://openalex.org/", "")}`
          );
        }
      },
      //clickStage: () => selectNode(null),
    });

    setSettings({
      nodeReducer: (node, data) => {
        let newData = {
          ...data,
        } as NodeDisplayData;
        const sigmaGraph = sigma.getGraph();
        if (hoveredNode) {
          if (
            node === hoveredNode ||
            (sigmaGraph.hasNode(hoveredNode) &&
              sigmaGraph.neighbors(hoveredNode).includes(node))
          ) {
            newData = {
              ...newData,
              color: data.color,

              label: data.label,
              highlighted: hoveredNode === node,
              forceLabel: true,
              zIndex: 1,
            };
          } else {
            newData = {
              ...newData,
              color: "#ddd",
              label: "",
            };
          }
        }
        return newData;
      },
      edgeReducer: (edge, data) => {
        const sigmaGraph = sigma.getGraph();
        let newData = { ...data };
        if (hoveredNode) {
          if (!sigmaGraph.extremities(edge).includes(hoveredNode)) {
            newData = { ...newData, color: "#FFF" };
          }
        }
        return newData;
      },
    });
  }, [sigma, registerEvents, hoveredNode, setSettings, setSearchParams]);

  return null;
};
