import axios from "axios";
import { FC } from "react";
import AsyncSelect from "react-select/async";
import { OpenAlexApiResponse, OpenAlexConcept } from "../types";

const promiseOptions = async (query: string) => {
  if (query !== "") {
    const response = await axios.get<OpenAlexApiResponse<OpenAlexConcept>>(
      "https://api.openalex.org/autocomplete/concepts",
      {
        method: "GET",
        params: {
          q: query,
        },
      }
    );
    if (response.data) {
      console.log(
        `got ${response.data.results.length} on ${response.data.meta.count}`
      );

      return response.data.results.map((r) => ({
        label: r.display_name,
        value: r,
      }));
    }
  }
  return [];
};

const ConceptsSelect: FC<{
  id?: string;
  concepts: OpenAlexConcept[];
  onChange: (concepts: OpenAlexConcept[]) => void;
}> = ({ onChange, concepts, id }) => (
  <AsyncSelect
    id={id}
    placeholder="search and select concepts"
    value={concepts.map((r) => ({
      label: r.display_name,
      value: r,
    }))}
    cacheOptions
    loadOptions={promiseOptions}
    isMulti
    onChange={(options) => {
      onChange(
        (Array.isArray(options) ? options : [options]).map((o) => o.value)
      );
    }}
  />
);
export default ConceptsSelect;
