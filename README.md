# culturIA

## Prepare environement

- install python > 3.6

```
pyenv install 3.10.1
```

Pyenv is one of the many ways to handle python version and env. Check out [installation documentation](https://github.com/pyenv/pyenv#installation).

- create a virtualenv

```
pyenv virtualenv 3.10.1 culturIA
```

For virtualenv to work from pyenv you need to install [virtualenv plugin](https://github.com/pyenv/pyenv-virtualenv)

- activate virtualenv

```
pyenv activate culturIA
```

- install dependencies

```
pip install -r requirements.txt
```

## How to use?

### Explore set of keywords

- add a list of queries in `data/search_queris.csv` with one column named `query`
- run `python search_queries.py`
- analyze results in `data/exploration/queries_overview.csv`
- see the first 200 results by query in `data/exploration/{query}.json`
